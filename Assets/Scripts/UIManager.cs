using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Slider volume;
    public AudioMixer mixer;
    public Toggle fullScreen;
    private void Awake()
    {
        volume.onValueChanged.AddListener(ChangeVolumeMaster);
        fullScreen.onValueChanged.AddListener(ChangeFullScreen);
    }
    public void SceneGame()
    {
        SceneManager.LoadScene("Game");
    }
    public void SceneInstruccions()
    {
        SceneManager.LoadScene("Instruccions");
    }
    public void SceneMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public void SceneResultats()
    {
        SceneManager.LoadScene("Resultats");
    }

    public void Sortir()
    {
        Application.Quit();
    }
    public void ChangeVolumeMaster(float v)
    {
        mixer.SetFloat("VolMaster", v);
    }
    public void ChangeFullScreen(bool fullscreen)
    {
        Debug.Log(fullscreen);
        Screen.fullScreen = fullscreen;
    }
}
