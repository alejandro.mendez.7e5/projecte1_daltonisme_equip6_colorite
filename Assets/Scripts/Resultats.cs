using UnityEngine;
using UnityEngine.UI;
using System;

public class Resultats : MonoBehaviour
{
    private int _puntuacioRed;
    private int _puntuacioGreen;
    private int _puntuacioBlue;
    private string _resultats = "";
    private string _resultats2 = "";
    private string _resultats3 = "";
    // Start is called before the first frame update
    void Start()
    {
        _puntuacioRed = PlayerPrefs.GetInt("Punts1");
        _puntuacioGreen = PlayerPrefs.GetInt("Punts2");
        _puntuacioBlue = PlayerPrefs.GetInt("Punts3");
        //Debug.Log(_resultats);
        Puntuacio();
        Puntuacio2();
        Puntuacio3();
        this.gameObject.GetComponent<Text>().text = _resultats + "\n"+"\n" + _resultats2+"\n" + "\n" + _resultats3;
        Debug.Log(_puntuacioRed);
        Debug.Log(_puntuacioGreen);
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void Puntuacio()
    {
        if( _puntuacioRed == 0 || _puntuacioRed == 10 || _puntuacioRed == 20 || _puntuacioRed == 30)
        {
            _resultats += "Has encertat un " + _puntuacioRed.ToString()+ "%, del nivell vermell. Tens daltonisme tipus Deuteranopia greu";
        }
        else if (_puntuacioRed == 40 || _puntuacioRed == 50 || _puntuacioRed == 60 || _puntuacioRed == 70)
        {
            _resultats += "Has encertat un " + _puntuacioRed.ToString() + "%, del nivell vermell. Tens daltonisme tipus Deuteranopia lleu";
        }
        else
        {
            _resultats += "Has encertat un " + Convert.ToString(_puntuacioRed) + "%, del nivell vermell. No tens daltonisme";
        }
    }

    public void Puntuacio2()
    {
        if (_puntuacioGreen == 0 || _puntuacioGreen == 10 || _puntuacioGreen == 20 || _puntuacioGreen == 30)
        {
            _resultats2 += "Has encertat un " + _puntuacioGreen.ToString() + "%, del nivell verd. Tens daltonisme tipus protanopia greu";
        }
        else if (_puntuacioGreen == 40 || _puntuacioGreen == 50 || _puntuacioGreen == 60 || _puntuacioGreen == 70)
        {
            _resultats2 += "Has encertat un " + _puntuacioGreen.ToString() + "%, del nivell verd. Tens daltonisme tipus protanopia lleu";
        }
        else
        {
            _resultats2 += "Has encertat un " + Convert.ToString(_puntuacioGreen) + "%, del nivell verd. No tens daltonisme";
        }
    }
    public void Puntuacio3()
    {
        if (_puntuacioBlue == 0 || _puntuacioBlue == 10 || _puntuacioBlue == 20 || _puntuacioBlue == 30)
        {
            _resultats3 += "Has encertat un " + _puntuacioBlue.ToString() + "%, del nivell blau. Tens daltonisme tipus Tritanopia greu";
        }
        else if (_puntuacioBlue == 40 || _puntuacioBlue == 50 || _puntuacioBlue == 60 || _puntuacioBlue == 70)
        {
            _resultats3 += "Has encertat un " + _puntuacioBlue.ToString() + "%, del nivell blau. Tens daltonisme tipus Tritanopia lleu";
        }
        else
        {
            _resultats3 += "Has encertat un " + Convert.ToString(_puntuacioBlue) + "%, del nivell blau. No tens daltonisme";
        }
    }
}
