using Unity.VisualScripting;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = System.Random;
using UnityEngine.UI;

public class Matriz2 : MonoBehaviour
{
    [SerializeField]
    private GameObject[,] _matrix;
    [SerializeField]
    private GameObject _square;
    Random rnd = new Random();
    public GameObject[,] colors;
    public Camera camera;
    private RaycastHit2D raycastHit;

    [SerializeField] private static float r = 1f;
    [SerializeField] private static float g = 0f;
    [SerializeField] private static float a = 1f;
    [SerializeField] private static float b = 0f;
    private Color punt = new(r, g, b, a);
    private GameObject[,] _cubo;

    private AudioSource _audioSource;
    public AudioClip soundVictory;
    public AudioClip soundLoss;
    public AudioClip clickOnRight;
    private int _win1;
    private int _win2;
    private int _lose;
    private float _timer;
    private bool _click = true;
    private bool sound = false;

    private Color[] _tonalitySquare;
    private Color[] _tonalitySquareMini;
    public float time = 0;
    public float maxTime = 0.1f;

    public Text progressionLevelText;
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _win1 = PlayerPrefs.GetInt("Punts1");
        _tonalitySquare = new Color[] { new Color(136f / 255f, 8f / 255f, 8f / 255f), Color.red, new Color(238f / 255f, 75f / 255f, 43f / 255f), new Color(165f / 255, 42f / 255, 42f / 255) };
        _tonalitySquareMini = new Color[] { Color.green, new Color(50f / 255f, 205f / 255f, 50f / 255f), new Color(34f / 255f, 139f / 255f, 34f / 255f), new Color(0, 128f / 255f, 0) };
        camera = Camera.main;
        _matrix = new GameObject[18, 18];
        colors = new GameObject[_matrix.GetLength(0), _matrix.GetLength(1)];
        GameObject cuadrado = _square;


        for (int i = 0; i < _matrix.GetLength(0); i++)
        {
            for (int j = 0; j < _matrix.GetLength(1); j++)
            {
                colors[i, j] = Instantiate(_square, new Vector3(j, i, 0), Quaternion.identity);
            }
        }
        Cubo();
        
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        if (time > maxTime && _click)
        {
            ChangeTonality();
            time = 0;
        }

        if (_click)
        {
            if (Input.GetMouseButtonDown(0))
            {
                raycastHit = Physics2D.Raycast(camera.ScreenToWorldPoint(Input.mousePosition), new Vector3(0, 0, 1f));

                if (raycastHit.collider == null)
                {
                    Debug.Log("Hss clicado fuera del cuadrado");

                }
                else
                {
                    if (raycastHit.collider.tag == "Square2")
                    {
                        ChangeSquares();
                        Cubo();
                        r += 0.1f;
                        g -= 0.1f;
                        punt = new Color(r, g, b, a);
                        for (int i = 0; i < _tonalitySquareMini.Length; i++)
                        {

                            _tonalitySquareMini[i] += new Color(+0.09f, -0.09f, 0);
                        }
                        _win2 += 10;
                        if (_lose > 0) _lose--;
                        progressText();
                        _audioSource.clip = clickOnRight;
                        _audioSource.Play();
                    }
                    else
                    {
                        _lose += 1;
                        Debug.Log("Intentalo de nuevo");
                    }
                }
            }
        }

        if (_win2 == 100)
        {
            _click = false;
        
            if (!sound)
            {
                Debug.Log("Estoy sonando");
                _audioSource.clip = soundVictory;
                _audioSource.Play();
                sound = true;
            }
            _timer += Time.deltaTime;
            if (_timer > 6f)
            {
                NextScene();
            }
        }
        else if (_lose > 1)
        {
            _click = false;
            if (!sound)
            {
                Debug.Log("Estoy sonando");
                _audioSource.clip = soundLoss;
                _audioSource.Play();
                sound = true;
            }
            _timer += Time.deltaTime;
            if (_timer > 3f)
            {
                NextScene();
            }
        }

    }


    public void Cubo()
    {
        _cubo = new GameObject[3, 3];
        int coordX = rnd.Next(0, _matrix.GetLength(0) - 2);
        int coordY = rnd.Next(0, _matrix.GetLength(1) - 2);

        for (int i = coordX; i < coordX + 3; i++)
        {
            for (int j = coordY; j < coordY + 3; j++)
            {
                colors[i, j].GetComponent<SpriteRenderer>().color = punt;
                colors[i, j].gameObject.tag = "Square2";
            }
        }
    }

    public void ChangeSquares()
    {
        Debug.Log(colors);
        for (int i = 0; i < _matrix.GetLength(0); i++)
        {
            for (int j = 0; j < _matrix.GetLength(1); j++)
            {
                colors[i, j].GetComponent<SpriteRenderer>().color = Color.red;
                colors[i, j].gameObject.tag = "Untagged";
            }
        }
    }

    public void ChangeTonality()
    {


        for (int i = 0; i < _matrix.GetLength(0); i++)
        {
            for (int j = 0; j < _matrix.GetLength(1); j++)
            {
                int pickColor = rnd.Next(0, 3);
                if (colors[i, j].gameObject.tag == "Untagged")
                {
                    colors[i, j].GetComponent<SpriteRenderer>().color = _tonalitySquare[pickColor];
                }
                if (colors[i, j].gameObject.tag == "Square2")
                {
                    colors[i, j].GetComponent<SpriteRenderer>().color = _tonalitySquareMini[pickColor];
                }
            }
        }



    }
    private void progressText()
    {
        progressionLevelText.text = "Completat: " + _win2 + "%";
    }

    private void NextScene()
    {
        PlayerPrefs.SetInt("Punts1", _win1);
        PlayerPrefs.SetInt("Punts2", _win2);
        SceneManager.LoadScene("Game3");
    }
}
